export default () => ({
  // Login page
  loginTitle: 'Spartan Dashboard',
  loginInfoBlocks: [
    {icon: 'lock_outline', header: 'Authentication', text: 'Login with your Rotterdam University username and password.'},
    {icon: 'security', header: 'Security', text: 'For security reasons, please log out and exit your web browser when you are done accessing services that require authentication!'}
  ],

  // Layout
  toolbarTitle: '',
  toolbarButtons: [],
  snackbarMessage: '',

  // Navigation
  navigationTitle: 'Spartan Dashboard',
  navigationItems: [
    { title: 'Dashboard', icon: 'dashboard', link: '/' },
    { title: 'Schedule', icon: 'today', link: '/schedule' },
    { title: 'Notifications', icon: 'contact_support', link: '/notifications' },
    { title: 'Rooms', icon: 'meeting_room', link: '/rooms' },
    { title: 'Calendars', icon: 'view_day', link: '/calendar' }
  ],

  // Dashboard
  DashboardToolbarTitle: 'Spartan Dashboard',

  // Schedule
  ScheduleOverviewToolbarTitle: 'HR Schedule',

  // Notifications
  NotificationsOverviewToolbarTitle: 'All Notifications',

  // Rooms
  RoomsOverviewToolbarTitle: 'All Rooms',

  // Calendar
  CalendarOverviewToolbarTitle: 'All Rooms Calendars',

  DemoToolbarTitle: 'Demo'
})
