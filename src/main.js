// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import store from './store'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import Raven from 'raven-js'
import RavenVue from 'raven-js/plugins/vue'
import Axios from 'axios'

// Sentry (Raven based) error tracking
if (process.env.NODE_ENV === 'production') {
  Raven
    .config('https://1e35e97a16d44c19873ca5dcf7aeebb7@sentry.io/301362')
    .addPlugin(RavenVue, Vue)
    .install()
}

// Axios http client default config
const axiosConfig = {
  baseURL: `${process.env.API_URL}`,
  headers: {'Accept': 'application/json'}
}
Vue.$axios = Axios.create(axiosConfig)

// vuetify component based frontend framework
Vue.use(Vuetify)

// Additional config
Vue.config.productionTip = false
store.dispatch('auth/initialize')

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
