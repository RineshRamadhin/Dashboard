'use strict'

const childProcess = require('child_process')

// build-time git commit hash
let commit_hash = childProcess.execSync("git rev-parse HEAD").toString().trim()
commit_hash = "'" + commit_hash + "'"

// built-time unix timestamp
let unix_timestamp = Date.now().toString()
unix_timestamp = "'" + unix_timestamp + "'"

module.exports = {
  NODE_ENV: '"production"',
  API_URL: `"http://145.24.222.157:8080"`,
  HR_URL: `"https://hint.hr.nl/xsp/public/InfApp/2018gr5/getLokaalRooster.xsp?sharedSecret=0904181-6otYcvUpCPTe7Jze69qiMm7HbEaR62niXez"`,
  BUILD_TIME_COMMIT_HASH: commit_hash,
  BUILD_TIME_UNIX_TIMESTAMP: unix_timestamp
}
